var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient, test = require('assert');
var ObjectID = require('mongodb').ObjectID;
/*multer to manage images*/
var multer = require("multer");
var upload = multer({dest: "uploads/"});

var loggedin =  false;
var loggedinas = "none";

var id=0;
var id2=0;
var id3=0;

var checked=false;

var cartArray = new Array();
var yhteishinta_cart = 0;
var cartpricetag;
var cartamounts=0;


/* GET home page. */
router.get('/', function(req, res, next) {
    var db = req.db;
    var collection = db.get('usercollection');
    var collection2 = db.get('productcollection');
    var collection3 = db.get('objectcollection');
    collection.find({},{},function(e,docs){
      collection2.find({},{},function(e,docs2){
        collection3.find({},{},function(e,docs3){
            id=docs.length;
            id2=docs2.length;
            res.render('index.jade', {
                title: "Wut",
                "userlist" : docs,
                "productlist": docs2,
                "objectlist": docs3,
                "loggedin": loggedin,
                "checked": checked
            });
          });
        });
    });
});

router.get('/helloworld', function(req, res) {
    res.render('helloworld', { title: 'Hello, World!' });
});

router.get('/home', function(req, res) {
    res.redirect("/");
});
router.get('/news', function(req, res) {
    res.render('news.jade', { title: 'Hello, World!' });
});
router.get('/books', function(req, res) {
    res.render('books.jade', { title: 'Hello, World!' });
});

router.get('/store', function(req, res) {
    res.render('store.jade', { title: 'Hello, World!' });
});
router.get('/testtarget', function(req, res){
    res.render('testtarget', { title: 'Hello, World!' });
});

router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');
    collection.find({},{},function(e,docs){
        res.render('userlist', {
            "userlist" : docs
        });
    });
});

router.get('/newuser', function(req, res) {
    res.render('newuser', { title: 'Add New User' });
});

router.get('/components', function(req, res) {
    res.render('components.jade', { title: 'Components' });
});

router.post('/edit/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('productcollection');
    var selectedaction = req.params.id;
        if(loggedin) {
            if(selectedaction == "productname") {
                collection.update(
                    {_id: req.body.editthisname},
                    {$set: {productname: req.body.editname}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            if(selectedaction == "addimage"){
                collection.update(
                    {_id: req.body.addtothisprod},
                    {$set: {additionalimage: req.body.oneimage}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            if(selectedaction == "productdesc"){
                collection.update(
                    {_id: req.body.editthisdesc},
                    {$set: {description: req.body.editdesc}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            if(selectedaction == "price"){
                collection.update(
                    {_id: req.body.editthisprice},
                    {$set: {price: req.body.editprice}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            if(selectedaction == "amount"){
                collection.update(
                    {_id: req.body.editthisamount},
                    {$set: {amount: req.body.editamount}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            if(selectedaction == "type"){
                var isproductBoolean = Boolean(req.body.isproductselection);
                collection.update(
                    {_id: req.body.editthistype},
                    {$set: {isproduct: isproductBoolean}})
                    .then((updatedDoc) => {
                    res.redirect('back');
                });}
            }
        else {res.redirect('back');}
});

router.get('/iframe/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');
    var collection2 = db.get('productcollection');
    var collection3 = db.get('objectcollection');
    collection.find({},{},function(e,docs){
      collection2.find({},{},function(e,docs2){
        collection3.find({},{},function(e,docs3){
            id=docs.length;
            id2=docs2.length;
            id3=docs3.length;
            var selectedurl = req.params.id;
            res.render('iframe.jade', {
                title: "Wut",
                "userlist" : docs,
                "productlist": docs2,
                "objectlist": docs3,
                "selectedurl": selectedurl,
                "loggedin": loggedin
                });
            });
        });
    });
});
router.get('/iframe/books/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');
    var collection2 = db.get('productcollection');
    var collection3 = db.get('objectcollection');
    collection.find({},{},function(e,docs){
      collection2.find({},{},function(e,docs2){
        collection3.find({},{},function(e,docs3){
            id=docs.length;
            id2=docs2.length;
            id3=docs3.length;
            var selectedurl = "books/"+req.params.id;
            res.render('iframe.jade', {
                title: "Wut",
                "userlist" : docs,
                "productlist": docs2,
                "objectlist": docs3,
                "selectedurl": selectedurl,
                "loggedin": loggedin
                });
            });
        });
    });
});

router.get('/product/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');
    var collection2 = db.get('productcollection');
    var collection3 = db.get('objectcollection');
    collection.find({},{},function(e,docs){
      collection2.find({},{},function(e,docs2){
        collection3.find({},{},function(e,docs3){
            id=docs.length;
            id2=docs2.length;
            id3=docs3.length;
            var selectedid = req.params.id;
            res.render('product.jade', {
                title: "Wut",
                "userlist" : docs,
                "productlist": docs2,
                "objectlist": docs3,
                "selectedid": selectedid,
                "loggedin": loggedin,
                "checked": checked
                });
            });
        });
    });
});

router.post('/login', function(req, res) {
  var db = req.db;
  var userEmail = req.body.loginEmail;
  var userPassword = req.body.loginPasswd;
  var collection = db.get('usercollection');
      collection.find({
          "email" : userEmail,
          "password": userPassword
      }).then(function () {
        loggedin = true;
        loggedinas = userEmail;
      });
  res.redirect('back');
});

router.post('/logout', function(req, res) {
    loggedin = false;
    loggedinas= "none";
    res.redirect('back');
});

router.post('/changewidth/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('productcollection');
    var selectedid = req.params.id;
    if(checked){checked=false;}
    else{checked=true;}
    if(checked){
        collection.update(
        {_id: ObjectID(selectedid)},
        {$set: {width: 2}})
        .then((updatedDoc) => {
        res.redirect('back');
        });}
    else{
        collection.update(
        {_id: ObjectID(selectedid)},
        {$set: {width: 1}})
        .then((updatedDoc) => {
        res.redirect('back');
        });}
});

router.post('/adduser', function(req, res) {
    var db = req.db;
    var userEmail = req.body.signupEmail;
    var userPassword = req.body.signupPasswd;
    var collection = db.get('usercollection');
    collection.find({},{},function(e,docs){
        id=docs.length;
    });
    collection.insert({
        "email" : userEmail,
        "password": userPassword
    }, function (err, doc) {
        if (err) {
            res.send("There was a problem adding the information to the database.");
        }
        else {
            res.redirect('back');
        }
    });
});

router.post('/deluser', function(req, res) {
    var db = req.db;
    MongoClient.connect('mongodb://localhost:27017/nodetest1', function(err, db) {
              var collection = db.collection("usercollection");
              collection.removeOne({_id :ObjectID(req.body.delthis)}).then(function(r) {
                    db.close();
                  });
              });
    res.redirect("/");
});

router.post('/upload',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end("File is uploaded");
    });
});

router.post('/addproduct', function(req, res) {
    var name = req.body.oneimage + "-" + Date.now() + ".png";
    var storage =   multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './public/img');
      },
      filename: function (req, file, callback) {
        callback(null, name);
      }
    });
    var upload = multer({ storage : storage}).single("oneimage");

    router.get('/',function(req,res){
          res.sendFile(__dirname + "/index.html");
    });
    var imagedata=req.files;
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
    });
    var db = req.db;
    var prodName = req.body.productname;
    var prodDesc = req.body.description;
    var prodPrice = req.body.price;
    var prodAmount = req.body.amount;
    var isproductBoolean = Boolean(req.body.producttype);
    /* polku + nimi tallennetaa */
    var prodImage = "/img/" + name;
    var collection = db.get('productcollection');
    collection.find({},{},function(e,docs){id=docs.length;});
    collection.insert({
        "productname" : prodName,
        "description" : prodDesc,
        "price": prodPrice,
        "amount" : prodAmount,
        "isproduct" : isproductBoolean,
        "width": 1,
        "image": prodImage
    }, function (err, doc) {
        if (err) {
            res.send("There was a problem adding the information to the database.");
        }
        else {
            res.redirect("/");
        }
    });
});

router.post('/delprod', function(req, res) {
    var db = req.db;
    MongoClient.connect('mongodb://localhost:27017/nodetest1', function(err, db) {
              var collection = db.collection("productcollection");
              collection.removeOne({_id :ObjectID(req.body.delthisprod)}).then(function(r) {
                    db.close();
                  });
              });
    res.redirect("/");
  });

module.exports = router;

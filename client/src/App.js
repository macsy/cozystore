import React, { Component } from 'react';
import { Button, Card, CardText, FABButton, Icon,
Layout, Header, Drawer, Navigation,
Content, Textfield, HeaderRow, HeaderTabs,
Tab, Footer, FooterSection,
FooterDropDownSection, FooterLinkList,
Grid, Cell, CardTitle, CardActions, CardMenu,
IconButton, Tooltip} from 'react-mdl';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
        super(props)
        this.state = { activeTab: 0 };
    }
  render() {
    return (
      <div className="App">
          <head>
            <meta charset="utf-8"></meta>
            <meta name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"></meta>
            <meta name="description" content=""></meta>
            <meta name="author" content=""></meta>
            <title>Cozy Store</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"></link>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap-theme.min.css"></link>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en"></link>
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
            <link rel="stylesheet" href="$$hosted_libs_prefix$$/$$version$$/material.cyan-light_blue.min.css"></link>
          </head>
          <Layout fixedHeader>
            <Header>
                <HeaderRow title="Title" />
                <HeaderTabs ripple activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })}>
                    <Tab>Tab1</Tab>
                    <Tab>Tab2</Tab>
                    <Tab>Tab3</Tab>
                    <Tab>Tab4</Tab>
                    <Tab>Tab5</Tab>
                    <Tab>Tab6</Tab>
                </HeaderTabs>
            </Header>
            <Drawer title="Title">
                <Navigation>
                    <a href="#">Link</a>
                    <a href="#">Link</a>
                    <a href="#">Link</a>
                    <a href="#">Link</a>
                </Navigation>
            </Drawer>
            <Content>
                <div className="page-content">
                  <p>Content for the tab: {this.state.activeTab+1}</p>
                  <Grid>
                      <Cell col={4} tablet={4} phone={12}>
                        <Card shadow={0} style={{width: '256px', height: '380px', background: 'url(http://www.dreamcozy.com/website-2015-01/wp-content/uploads/2017/02/Mokkapala_Pysty-829x1024.jpg) center / cover', margin: 'auto'}}>
                            <CardTitle expand />
                            <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
                                <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
                                    Example Product
                                </span>
                            </CardActions>
                        </Card>
                      </Cell>
                      <Cell col={4} tablet={4} phone={12}>
                        <Card shadow={0} style={{width: '256px', height: '380px', background: 'url(http://www.dreamcozy.com/website-2015-01/wp-content/uploads/2015/12/AidostiKaunis-1-840x1002.jpg) center / cover', margin: 'auto'}}>
                            <CardTitle expand />
                            <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
                                <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
                                    Another One
                                </span>
                            </CardActions>
                        </Card>
                      </Cell>
                  </Grid>

                </div>
            </Content>


            <Footer size="mini">
                <FooterSection type="left" logo="Cozy Publishing inc. All Rights Reserved">
                    <FooterLinkList>
                      <a href="#">Help</a>
                      <a href="#">Privacy & Terms</a>
                    </FooterLinkList>
                    </FooterSection>
            </Footer>
        </Layout>
      </div>
    );
  }
}

export default App;

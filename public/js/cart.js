var cartArray = new Array();
var yhteishinta_cart = 0;
var pricetag;
var amounts=0;

      $(function(){
        var badgecookie = getCookieBadge();
        amounts = badgecookie;
        document.getElementById("cartcontent").setAttribute("data-badge", amounts);
      });
      function getCookieBadge() {
          var language = "badge=";
          var ca = document.cookie.split(';');
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(language) == 0) {
                  return c.substring(language.length, c.length);
              }
          }
          return "";
      }
      function Product (a_name, a_price)
      {
        this.name = a_name;
        this.amount = parseFloat(1);
        this.price = parseFloat(a_price);
      }

      function addToCart(a_productId, a_price)
      {
        amounts++;
        document.getElementById("cartcontent").setAttribute("data-badge", amounts);
        document.cookie = "badge="+amounts;

        var newProduct = new Product(a_productId, a_price);
        var productExists = existsInArray(cartArray, newProduct.name);

        if(productExists == -1)
        {

          cartArray.push(newProduct);
          console.log("Added product");

        }else{

          cartArray[productExists].amount++;
          console.log("Product quantity increased");

        }
        for(var i = 0; i < cartArray.length; i++) console.log(cartArray[i].name);
        //antaa hinnan nimen sijasta
        getCart();

        cartToCookie("cart", cartArray, 5);
      }

      function existsInArray(array, productName)
      {

        for(var x = 0; x < cartArray.length; x++)
        {
          if(cartArray[x].name == productName) return x;
        }

        return -1;
      }

      function getCart()
      {
        yhteishinta_cart = 0;
        //variablet cart ja pricetag luodaan etukäteen?
        var cart = document.getElementById("cart");
        pricetag = document.getElementById("pricetag");

        var iterator;
        var item;
        var button;
        var buttonText;

        var headerTr;
        var tr;

        var td = Array();
        var numberOfColumns = 4;
        var columnNames = ["Product ","Quantity ","Price", ""];

        //Alustetaan cartti ettei tule duplicate shittiä sinne
        while (cart.firstChild)
        {
          cart.removeChild(cart.firstChild);
        }



        //Luodaan otsikot koriin
        headerTr = document.createElement('tr');
        headerTr.setAttribute("class","mdl-grid mdl-navigation");

        //Haetaan keksi
        var arr = getCartCookie();

        for(var i = 0; i < arr.length; i=i+3)
        {
          var product = new Product(arr[i], arr[i+1]);
          product.amount = arr[i+2];

          if(existsInArray(cartArray, product.name) == -1 && product.name != "")cartArray.push(product);


        }

        for(var i = 0; i < numberOfColumns; i++)
        {
          td.push(document.createElement('td'));
          td[i].appendChild(document.createTextNode(columnNames[i]));
          //var divtab=document.createElement('div');
          //divtab.setAttribute("class","mdl-layout-spacer");
          td[i].setAttribute("class","mdl-cell mdl-color-text--white mdl-cell--3-col");
          td[i].setAttribute("width","25%");
          //headerTr.appendChild(divtab);
          headerTr.appendChild(td[i]);
        }
        cart.appendChild(headerTr);


        for(iterator = 0; iterator < cartArray.length; iterator++)
        {
          var tr = document.createElement('tr');
          tr.setAttribute("class","mdl-grid mdl-navigation");

          //Tuotteen nimen näyttäminen
          td[0] = document.createElement('td');
          item = document.createElement('a');
          item.appendChild(document.createTextNode(cartArray[iterator].name));
          item.setAttribute("href", "#");
          item.setAttribute("class", "mdl-color-text--white mdl-cell--3-col");
          td[0].appendChild(item);

          td[1] = document.createElement('td');
          td[1].appendChild(document.createTextNode(cartArray[iterator].amount));
          td[1].setAttribute("class", "amountcounter");


          //Hinnan näyttäminen
          var price = cartArray[iterator].amount * cartArray[iterator].price;

          console.log("price: " + price);
          td[2] = document.createElement('td');
          td[2].appendChild(document.createTextNode(price + "€"));
          yhteishinta_cart += price;
          console.log("Yhteishinta:" + yhteishinta_cart);
          while(pricetag.firstChild) pricetag.removeChild(pricetag.firstChild);
          pricetag.appendChild(document.createTextNode("Yht: " + yhteishinta_cart + "€"));
          //Delete item buttonin näyttäminen
          td[3] = document.createElement('td');
          button = document.createElement('button');
          buttonText = document.createTextNode("Remove");
          button.setAttribute("class","mdl-button mdl-js-button mdl-button--raised mdl-color--red-800 mdl-color-text--white");
          button.appendChild(buttonText);

          button.setAttribute("onClick", "removeItemFromCart("+iterator+"); removeFromBadge("+cartArray[iterator].amount+")");

          td[3].appendChild(button);

          for(var i = 0; i < td.length; i++)
          {
            tr.appendChild(td[i]);
            td[i].setAttribute("style","width:25%");
          }

          cart.appendChild(tr);

        }

      }

      //Poistaa indexin perusteella systeemin korista
      function removeItemFromCart(itemIndex)
      {
        yhteishinta_cart = yhteishinta_cart - (cartArray[itemIndex].price * cartArray[itemIndex].amount);
        if(cartArray.length < 1) while(pricetag.firstChild) pricetag.removeChild(pricetag.firstChild);
        cartArray.splice(itemIndex,1);
        cartToCookie("cart", cartArray, 5);
        console.log("yhteishinta: " + yhteishinta_cart);
        getCart();
      }

      function cartToCookie(cname, cartArray, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        var content = "";

        for(var i = 0; i < cartArray.length; i++)
        {
          content = content + cartArray[i].name + ":" + cartArray[i].price + ":" + cartArray[i].amount;

          if(i < cartArray.length - 1) content = content + ":";
        }

        document.cookie = cname + "=" + content + ";" + expires + ";path=/";
      }

      function removeFromBadge(lkm){
        amounts -= lkm;
        document.getElementById("cartcontent").setAttribute("data-badge", amounts);
        document.cookie = "badge="+amounts;
      }

      function getCartCookie() {
        var name = "cart=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0)
            {
              var ra = c.substring(name.length, c.length).split(':');

              return ra;
            }
        }
        return "";
      }
